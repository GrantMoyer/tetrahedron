#![feature(array_map)]
use glium::{
	draw_parameters::DrawParameters,
	glutin::{
		dpi::LogicalSize,
		event::{DeviceEvent, Event, KeyboardInput, VirtualKeyCode, WindowEvent, ElementState, MouseButton},
		event_loop::{ControlFlow, EventLoop},
		window::WindowBuilder,
		ContextBuilder,
	},
	implement_vertex,
	index::PrimitiveType,
	program::Program,
	uniforms::{UniformValue, Uniforms},
	Display, IndexBuffer, Surface, VertexBuffer,
};
use ultraviolet::{rotor::Rotor3, transform::Isometry3, vec::Vec3};
use std::io::Write;

struct TransformUniform {
	transform: Isometry3,
}

impl Uniforms for TransformUniform {
	fn visit_values<'a, F: FnMut(&str, UniformValue<'a>)>(&'a self, mut f: F) {
		let mat = self
			.transform
			.into_homogeneous_matrix()
			.as_component_array()
			.map(|v| v.as_array().clone())
			.clone();
		f("transform", UniformValue::Mat4(mat));
	}
}

#[derive(Copy, Clone)]
struct Vert {
	position: [f32; 3],
}

implement_vertex!(Vert, position);

fn main() {
	let mut event_loop = EventLoop::new();
	let window_builder = WindowBuilder::new()
		.with_inner_size(LogicalSize::new(256.0, 256.0))
		.with_title("Tetrahedron");
	let context_builder = ContextBuilder::new();
	let display = Display::new(window_builder, context_builder, &event_loop)
		.expect("Failed to create OpenGL context");

	let program = Program::from_source(
		&display,
		include_str!("../shader/vert.glsl"),
		include_str!("../shader/frag.glsl"),
		None,
	)
	.expect("Failed to compile shaders");

	let draw_parameters = DrawParameters {
		..Default::default()
	};

	let mut transform = TransformUniform {
		transform: Isometry3::identity(),
	};

	#[rustfmt::skip]
	let verts = [
		Vec3::new( 1.0,  1.0,  1.0),
		Vec3::new(-1.0, -1.0,  1.0),
		Vec3::new( 1.0, -1.0, -1.0),
		Vec3::new(-1.0,  1.0, -1.0),
	]
	.iter()
	.map(|v| *v / 8.0f32.sqrt())
	.collect::<Vec<_>>();

	let vertex_buffer = VertexBuffer::new(
		&display,
		&verts
			.iter()
			.map(|v| Vert {
				position: v.as_array().clone(),
			})
			.collect::<Vec<_>>(),
	)
	.expect("Failed to create vertex buffer");

	#[rustfmt::skip]
	let indices: &[u8] = &[
		0, 2,
		2, 3,
		3, 0,

		0, 1,
		1, 2,
		2, 0,

		0, 3,
		3, 1,
		1, 0,

		1, 3,
		3, 2,
		2, 1,
	];
	let index_buffer = IndexBuffer::new(&display, PrimitiveType::LinesList, &indices)
		.expect("Failed to create index buffer");

	let mut mouse_pressed = false;

	event_loop.run(move |event, window, control_flow| match event {
		Event::WindowEvent {
			event: WindowEvent::CloseRequested,
			..
		} => {
			*control_flow = ControlFlow::Exit;
		}
		Event::DeviceEvent {
			event: DeviceEvent::MouseMotion { delta },
			..
		} => {
			if mouse_pressed {
				const SCALE: f32 = 0.01;
				let yz = -delta.1 as f32 * SCALE;
				let xz = delta.0 as f32 * SCALE;
				transform
					.transform
					.append_rotation(Rotor3::from_euler_angles(0.0, yz, xz));
				display.gl_window().window().request_redraw();
			}
		}
		Event::WindowEvent {
			event:
				WindowEvent::MouseInput {
					button: MouseButton::Left,
					state,
					..
				},
			..
		} => {
			mouse_pressed = state == ElementState::Pressed;
		}
		Event::WindowEvent {
			event:
				WindowEvent::KeyboardInput {
					input: KeyboardInput {
						virtual_keycode: Some(key),
						state: ElementState::Pressed,
						..
					},
					..
				},
			..
		} => {
			if (key == VirtualKeyCode::Return) {
				let mut f = std::fs::File::create("coords.txt").expect("Failed to open coords.txt");
				for vert in &verts {
					let projected = transform.transform.transform_vec(*vert);
					writeln!(f, "{}, {}", projected.x, projected.y);
				}
			}
		}
		Event::RedrawRequested(_) => {
			let mut frame = display.draw();
			frame.clear_color(1.0, 1.0, 1.0, 1.0);
			frame
				.draw(
					&vertex_buffer,
					&index_buffer,
					&program,
					&transform,
					&draw_parameters,
				)
				.expect("Failed to draw tetrahedron");
			frame.finish().expect("Failed to swap buffer");
		}
		_ => (),
	});
}
