#version 330

out vec4 color;

void main() {
	float intensity = gl_FragCoord.z / 1.2;
	color = vec4(intensity, intensity, intensity, 1.0);
}
